//
//  MainViewController.m
//  Tap Plus Plus
//
//  Created by Matthew Berkland on 3/23/13.
//  Copyright (c) 2013 Matthew Berkland. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize txtCount;


//create a variable to hold onto the current count value
int count = 0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //initliaze this once
    count = 0;
    //set our label to the value above...
    txtCount.text = [NSString stringWithFormat:@"%d", count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) updateLabel{
    txtCount.text = [NSString stringWithFormat:@"%d", count];
}

-(void) addOne{
    count = count +1;
}
-(void) minusOne{
    count = count -1;
}
-(void) showAlert{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Can't go that high" message:@"NOOOOO...." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [av show];
}

- (IBAction)bigButtonPress:(id)sender {
    //every time we push button add one to the number
    [self addOne];
    if(count > 9){
        count = 9;//dont go above 9
        //our business logic doesn't allow this
        //don't do anything
        
        //show an alert view to not go above 9
        [self showAlert];
        
        return;
    }
    else{
        //set our label to the value above...
        txtCount.text = [NSString stringWithFormat:@"%d", count];
    }

}

- (IBAction)decrementPressed:(id)sender {
    //they pushed the red button, decrement our number!
    [self minusOne];
    if(count >=0){
        txtCount.text = [NSString stringWithFormat:@"%d", count];
    }
    else{
        count=0;//don't go below 0
        //can't go below 0....
        return;
        
    }
}
@end
