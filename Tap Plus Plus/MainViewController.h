//
//  MainViewController.h
//  Tap Plus Plus
//
//  Created by Matthew Berkland on 3/23/13.
//  Copyright (c) 2013 Matthew Berkland. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *txtCount;
- (IBAction)bigButtonPress:(id)sender;
- (IBAction)decrementPressed:(id)sender;

@end
