//
//  AppDelegate.m
//  Tap Plus Plus
//
//  Created by Matthew Berkland on 3/23/13.
//  Copyright (c) 2013 Matthew Berkland. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"

@implementation AppDelegate

@synthesize mainView;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    //init our view contorller
    mainView = [[MainViewController alloc] init];
    
    //set as root view
    self.window.rootViewController = mainView;
    
    //carry on...
    [self.window makeKeyAndVisible];
    return YES;
}

@end
